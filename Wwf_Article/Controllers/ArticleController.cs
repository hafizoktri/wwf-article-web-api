﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Wwf_Article.HelperCode;
using Wwf_Article.Models;

namespace Wwf_Article.Controllers
{
    //[EnableCors(origins: "http://localhost:57728/", headers: "*", methods: "*")]
    public class ArticleController : ApiController
    {
        private Wwf_ArticleEntities db = new Wwf_ArticleEntities();


        //GET api/article
        public IQueryable<Article> GetArticles()
        {
            return db.Articles;
        }


        //GET api/article/5
        public IHttpActionResult GetArticle(int id)
        {
            Article article = db.Articles.Find(id);

            if (article == null)
            {
                 return NotFound();
            }

            return Ok(article);
        }

        //POST api/article
        [Mime]
        public async Task<IHttpActionResult> Post()
        {

            var fileuploadPath = HttpContext.Current.Server.MapPath("~/ImageFolder");

            var multiFormDataStreamProvider = new MultiFileUploadProvider(fileuploadPath);

            await Request.Content.ReadAsMultipartAsync(multiFormDataStreamProvider);

            string uploadingFileName = multiFormDataStreamProvider.FileData.Select(x => x.LocalFileName).FirstOrDefault();

            Article article = new Article
            {
                Title = HttpContext.Current.Request.Form["Title"],
                Content = HttpContext.Current.Request.Form["Content"],
                FilePath = uploadingFileName,
                FileName = Path.GetFileName(uploadingFileName),
                FileLength = new FileInfo(uploadingFileName).Length,
                FileCreatedTime = DateTime.Now
            };

            db.Articles.Add(article);
            db.SaveChanges();

            return Ok();
        }

        //PUT api/article/5
        public IHttpActionResult Edit(int id, Article article)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (id != article.ID)
            {
                return BadRequest();
            }

            db.Entry(article).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArticleIdExists(id))
                {
                    return NotFound();
                }

                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        //DELETE api/article/5
        public IHttpActionResult Delete(int id)
        {
            Article article = db.Articles.Find(id);

            if (article == null)
            {
                return NotFound();
            }

            db.Articles.Remove(article);
            db.SaveChanges();

            return Ok(article);
        }

        private bool ArticleIdExists(int id)
        {
            return db.Articles.Count(e => e.ID == id) > 0;
        }
    }
}
