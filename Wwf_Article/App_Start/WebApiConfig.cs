﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Wwf_Article
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //Add Cors
            string origin = "http://localhost:57728/";
            EnableCorsAttribute cors = new EnableCorsAttribute(origin, "*", "GET, POST");

            config.EnableCors();

            // Web API configuration and services
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
