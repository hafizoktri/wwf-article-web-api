﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wwf_Article.Models
{
    public class ArticleModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public float FileLength { get; set; }
    }
}